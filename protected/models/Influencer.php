<?php

/**
 * This is the model class for table "{{Influencer}}".
 *
 * The followings are the available columns in table '{{Influencer}}':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $seeding_platform
 * @property integer $followers
 * @property string $handle_tiktok
 * @property string $handle_instagram
 * @property string $handle_youtube
 * @property integer $following_tier
 * @property integer $demographic
 * @property integer $segment
 * @property string $notes
 * @property integer $added_by_user_id
 * @property string $date_created
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property User $addedByUser
 */
class Influencer extends CActiveRecord
{
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;

	const SEEDING_PLATFORM_TIKTOK = 1;
	const SEEDING_PLATFORM_INSTAGRAM = 2;
	const SEEDING_PLATFORM_YOUTUBE = 3;
	const SEEDING_PLATFORM_TWITTER = 4;
	const SEEDING_PLATFORM_FACEBOOK = 5;

	const FOLLOWING_TIER_MICRO = 1;
	const FOLLOWING_TIER_MACRO = 2;

	const DEMOGRAPHIC_GENZ = 1;
	const DEMOGRAPHIC_MILLENIAL = 2;

	const SEGMENT_BEAUTY = 1;
	const SEGMENT_SKINFLUENCER = 2;
	const SEGMENT_LIFESTYLE = 3;
	const SEGMENT_PERSONALITY = 4;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{influencer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, seeding_platform, followers, following_tier, demographic, segment, added_by_user_id, notes', 'required'),
			array('seeding_platform, followers, following_tier, demographic, segment, added_by_user_id', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, handle_tiktok, handle_instagram, handle_youtube', 'length', 'max'=>128),
			array('notes, hyperlink_tiktok, hyperlink_instagram, hyperlink_youtube', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, seeding_platform, followers, handle_tiktok, handle_instagram, handle_youtube, hyperlink_tiktok, hyperlink_instagram, hyperlink_youtube, following_tier, demographic, segment, notes, added_by_user_id, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addedByUser' => array(self::BELONGS_TO, 'User', 'added_by_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'seeding_platform' => 'Seeding Platform',
			'followers' => 'Followers',
			'handle_tiktok' => 'Tiktok Handle',
			'handle_instagram' => 'Instagram Handle ',
			'handle_youtube' => 'Youtube Handle',
			'hyperlink_tiktok' => 'Tiktok Hyperlink',
			'hyperlink_instagram' => 'Instagram Hyperlink',
			'hyperlink_youtube' => 'Youtube Hyperlink',
			'following_tier' => 'Following Tier',
			'demographic' => 'Demographic',
			'segment' => 'Segment',
			'notes' => 'Notes',
			'added_by_user_id' => 'Added By User',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'full_name' => 'Full Name'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('seeding_platform',$this->seeding_platform);
		$criteria->compare('followers',$this->followers);
		$criteria->compare('handle_tiktok',$this->handle_tiktok,true);
		$criteria->compare('handle_instagram',$this->handle_instagram,true);
		$criteria->compare('handle_youtube',$this->handle_youtube,true);
		$criteria->compare('following_tier',$this->following_tier);
		$criteria->compare('demographic',$this->demographic);
		$criteria->compare('segment',$this->segment);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('added_by_user_id',$this->added_by_user_id);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Influencer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes() 
	{
		return [
			'notDeleted' => [
				'condition' => 'status != ' . self::STATUS_DELETED
			]
		];
	}

	public static function getSeedingPlatformList() 
	{
		return [
			self::SEEDING_PLATFORM_TIKTOK => 'TikTok',
			self::SEEDING_PLATFORM_INSTAGRAM => 'Instagram',
			self::SEEDING_PLATFORM_YOUTUBE => 'Youtube',
			// self::SEEDING_PLATFORM_TWITTER => 'Twitter',
			// self::SEEDING_PLATFORM_FACEBOOK => 'Facebook',
		];
	}

	public static function getSeedingPlatformLabel($key) {
		return self::getSeedingPlatformList()[$key];
	}

	public static function getFollowingTierList() 
	{
		return [
			self::FOLLOWING_TIER_MICRO => 'Micro',
			self::FOLLOWING_TIER_MACRO => 'Macro',
		];
	}

	public static function getFollowingTierLabel($key) {
		return self::getFollowingTierList()[$key];
	}

	public static function getDemographicList() 
	{
		return [
			self::DEMOGRAPHIC_GENZ => 'Gen Z',
			self::DEMOGRAPHIC_MILLENIAL => 'Millenial',
		];
	}

	public static function getDemographicLabel($key) {
		return self::getDemographicList()[$key];
	}

	public static function getSegmentList() 
	{
		return [
			self::SEGMENT_BEAUTY => 'Beuty',
			self::SEGMENT_SKINFLUENCER => 'Skinfluencer',
			self::SEGMENT_LIFESTYLE => 'Lifestyle',
			self::SEGMENT_PERSONALITY => 'Personality',
		];
	}

	public static function getSegmentLabel($key) {
		return self::getSegmentList()[$key];
	}

	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
			if ($this->isNewRecord) 
			{
				$this->date_created = date('Y-m-d H:i:s');
				$this->date_updated = date('Y-m-d H:i:s');
			}
			else 
			{
				$this->date_updated = date('Y-m-d H:i:s');
			}
		}

		return true;
	}
}
