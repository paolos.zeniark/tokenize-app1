<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->redirect(['influencer/index']);
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];

			// authenticate account access
			if ($model->validate())
			{
				// prepare request authetication data
				$postData = [
					'app_id' => Yii::app()->params['authAppId'],
					'account_id' => $model->user->id,
					'company_id' => $model->user->company_id,
				];
		
				// request jwt from authetication server, return error if invalid
				$response = json_decode($this->authenticate($postData));

				// verify response code, return error if invalid
				if ($response->code == 200)
				{
					// verify jwt, return error if invalid
					if (isset($response->jwt) && $response->jwt != '') 
					{
						// create https cookie using jwt
						ini_set("session.cookie_httponly", true);
						setcookie("auth_jwt", $response->jwt, (time() + (86400 * 30)), "/", "", "", "true");

						// redirect to target url
						$this->redirect(Yii::app()->user->returnUrl);
					}
					else
						Yii::app()->user->setFlash('error','Missing token in auth server!');
				}
				else 
					Yii::app()->user->setFlash('error', $response->message);

			}
		}

		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		if (isset($_COOKIE['auth_jwt'])) {
			unset($_COOKIE['auth_jwt']);
			setcookie('auth_jwt', '', time() - 3600, '/'); // empty value and old timestamp
		}

		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	protected function authenticate($postData)
	{
		$ch = curl_init(Yii::app()->params['authAppUri'] . '/authenticate.php');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		
		// execute!
		$response = curl_exec($ch);

		if (curl_errno($ch))
			throw new Exception(curl_error($ch));

		// close the connection, release resources used
		curl_close($ch);

		return $response;
	}
}