<?php

class InfluencerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'create','update','delete', 'seeds', 'export'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{	
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Influencer;

		if (isset($_POST['Influencer']))
		{
			$model->attributes = array_map('trim', $_POST['Influencer']);
			$model->added_by_user_id = Yii::app()->user->info->id;

			if ($model->validate())
			{
				if ($this->additionalFormValidation($model))
				{
					if ($model->save())
					{
						Yii::app()->user->setFlash('success', 'Item successfully created!');
						$this->redirect(array('view','id'=>$model->id));
					}
					else 
						Yii::app()->user->setFlash('error', 'Unable to save record!');
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Influencer']))
		{
			$model->attributes = array_map('trim', $_POST['Influencer']);
			if($model->save())
			{
				Yii::app()->user->setFlash('success', 'Item successfully updated.');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		$model->status = Influencer::STATUS_DELETED;

		if ($model->save())
		{
			Yii::app()->user->setFlash('success', 'Item successfully delete.');
			$this->redirect(['index']);
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new Influencer;

		$filterParams = $this->prepareFilterParams();
		$queryAdditionalCond = $this->prepareAdditionalCond($filterParams);

		$query = '
			SELECT infl.*, user.username FROM {{influencer}} AS infl
			INNER JOIN {{user}} AS user ON user.id = infl.added_by_user_id
			WHERE 1
			'. $queryAdditionalCond .'
			AND infl.status = '. Influencer::STATUS_ACTIVE .'
			ORDER BY infl.id DESC
		';

		$command = Yii::app()->db->createCommand($query);
		$influencers = $command->queryAll();

		$influencersDP = new CArrayDataProvider($influencers);
		$influencersDP->getPagination()->pageSize = 100;

		$this->render('index',array(
			'model' => $model,
			'filterParams' => $filterParams,
			'influencersDP'=>$influencersDP,
		));
	}

	public function actionExport() 
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="InfluencerExport.csv";');

		$filterParams = $this->prepareFilterParams();
		$queryAdditionalCond = $this->prepareAdditionalCond($filterParams);

		$query = '
			SELECT infl.*, user.username FROM {{influencer}} AS infl
			INNER JOIN {{user}} AS user ON user.id = infl.added_by_user_id
			WHERE 1
			'. $queryAdditionalCond .'
			AND infl.status = '. Influencer::STATUS_ACTIVE .'
			ORDER BY infl.id DESC
		';

		$command = Yii::app()->db->createCommand($query);
		$influencers = $command->queryAll();

		$influencersDP = new CArrayDataProvider($influencers);
		$influencersDP->getPagination()->pageSize = $influencersDP->totalItemCount;

		$exportColumns = [
			'Date Created',
			'Added By',
			'Full Name',
			'Seeding Plaform',
			'Followers',
			'TikTok Handle',
			'Instgram Handle',	
			'Youtube Handle',
			'Following Tier',	
			'Demographic',
			'Segment',
			'Notes'
		];

		$exportRows = [];

		$exportRows[] = $exportColumns;
		if (count($influencersDP->rawData) > 0) {
			foreach ($influencersDP->rawData as $data) {
				$dateCreated = date('Y-m-d', strtotime('now')) . ' @ ' . date('h:m A', strtotime('now'));
				$fullName = $data['first_name'] . ' ' . $data['last_name'];

				$exportRows[] = [
					$dateCreated,
					$data['username'], // Added By
					$fullName, // Full Name
					Influencer::getSeedingPlatformLabel($data['seeding_platform']), // Seeding Plaform
					$data['followers'], // Followers
					$data['handle_tiktok'], // TikTok Handle
					$data['handle_instagram'], // Instgram Handle
					$data['handle_youtube'], // Youtube Handle
					Influencer::getFollowingTierLabel($data['following_tier']), // Tier
					Influencer::getDemographicLabel($data['demographic']), // Demographic
					Influencer::getSegmentLabel($data['segment']), // Segment
					$data['notes'], // Notes
				];
			}
		}
		
		// Open a file in write mode ('w')
		// $fp = fopen('export/influencerExportData-'.date('Y-m-d').'.csv', 'w+');
		
		$fp = fopen('php://output', 'w');

		// Loop through file pointer and a line
		foreach ($exportRows as $fields) {
			fputcsv($fp, $fields);
		}
		
		fclose($fp);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Influencer('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Influencer']))
			$model->attributes=$_GET['Influencer'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Seeds data
	 */
	public function actionSeeds($key)
	{
		if ($key != '86f7e437')
			throw new CHttpException(403, 'Forbidden to access the page!');
		
		$itemCount = 20;
		for ($i = 0; $itemCount > $i; $i++) {
			$model = new Influencer;

			$model->first_name = 'Jonh';
			$model->last_name = 'Duh';
			$model->seeding_platform = Influencer::SEEDING_PLATFORM_YOUTUBE;
			$model->followers = 1000;
			$model->handle_tiktok = 'JohnDuTk';
			$model->handle_instagram = 'JohnDuIns';
			$model->handle_youtube = 'JohnDuYT';
			$model->hyperlink_tiktok = 'https://www.google.com/';
			$model->hyperlink_instagram = 'https://www.google.com/';
			$model->hyperlink_youtube = 'https://www.google.com/';
			$model->following_tier = Influencer::FOLLOWING_TIER_MICRO;
			$model->demographic = Influencer::DEMOGRAPHIC_GENZ;
			$model->segment = Influencer::SEGMENT_SKINFLUENCER;
			$model->notes = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do';
			$model->added_by_user_id = 25;
			
			$model->save(false);
		}

		echo 'Success! - '.strtotime('now');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Influencer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Influencer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Influencer $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='influencer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function prepareAdditionalCond($filterParams) 
	{
		$cond = '';

		if ($filterParams['date_created_from'] != '' && $filterParams['date_created_to'] != '')
			$cond .= ' AND DATE(infl.date_created) BETWEEN "'. $filterParams['date_created_from'] .'" AND "'. $filterParams['date_created_to'] .'"';

		if ($filterParams['full_name'] != '')
			$cond .= ' AND CONCAT(infl.first_name, " ", infl.last_name) LIKE "%'. $filterParams['full_name'] .'%"';

		if ($filterParams['added_by_user_id'] != '')
			$cond .= ' AND infl.added_by_user_id = ' . $filterParams['added_by_user_id'];			
			
		if (!empty($_GET['seeding_platform']))
			$cond .= ' AND infl.seeding_platform = ' . $filterParams['seeding_platform'];

		if (!empty($_GET['followers']))
			$cond .= ' AND infl.followers < ' . $filterParams['followers'];

		if (!empty($_GET['handle_tiktok']))
			$cond .= ' AND infl.handle_tiktok like "%' . $filterParams['handle_tiktok'] . '%"';

		if (!empty($_GET['handle_instagram']))
			$cond .= ' AND infl.handle_instagram like "%' . $filterParams['handle_instagram'] . '%"';

		if (!empty($_GET['handle_youtube']))
			$cond .= ' AND infl.handle_youtube like "%' . $filterParams['handle_youtube'] . '%"';

		if (!empty($_GET['following_tier']))
			$cond .= ' AND infl.following_tier like "%' . $filterParams['following_tier'] . '%"';

		if (!empty($_GET['demographic']))
			$cond .= ' AND infl.demographic = ' . $filterParams['demographic'];

		if (!empty($_GET['segment']))
			$cond .= ' AND infl.segment = ' . $filterParams['segment'];

		if (!empty($_GET['notes']))
			$cond .= ' AND infl.notes like "%' . $filterParams['notes'] . '%"';

		return $cond;
	}

	protected function prepareFilterParams()
	{
		$filterParams = [
			'date_created_from' => isset($_GET['date_created_from']) && trim($_GET['date_created_from']) != '' ? trim($_GET['date_created_from']) : '',
			'date_created_to' => isset($_GET['date_created_to']) && trim($_GET['date_created_to']) != '' ? trim($_GET['date_created_to']) : '',
			'added_by_user_id' => isset($_GET['added_by_user_id']) && trim($_GET['added_by_user_id']) != '' ? trim($_GET['added_by_user_id']) : '',
			'full_name' => isset($_GET['full_name']) && trim($_GET['full_name']) != '' ? trim($_GET['full_name']) : '',
			'seeding_platform' => isset($_GET['seeding_platform']) && trim($_GET['seeding_platform']) != '' ? trim($_GET['seeding_platform']) : '',
			'followers' => isset($_GET['followers']) && trim($_GET['followers']) != '' ? trim($_GET['followers']) : '',
			'handle_tiktok' => isset($_GET['handle_tiktok']) && trim($_GET['handle_tiktok']) != '' ? trim($_GET['handle_tiktok']) : '',
			'handle_instagram' => isset($_GET['handle_instagram']) && trim($_GET['handle_instagram']) != '' ? trim($_GET['handle_instagram']) : '',
			'handle_youtube' => isset($_GET['handle_youtube']) && trim($_GET['handle_youtube']) != '' ? trim($_GET['handle_youtube']) : '',
			'following_tier' => isset($_GET['following_tier']) && trim($_GET['following_tier']) != '' ? trim($_GET['following_tier']) : '',
			'demographic' => isset($_GET['demographic']) && trim($_GET['demographic']) != '' ? trim($_GET['demographic']) : '',
			'segment' => isset($_GET['segment']) && trim($_GET['segment']) != '' ? trim($_GET['segment']) : '',
			'notes' => isset($_GET['notes']) && trim($_GET['notes']) != '' ? trim($_GET['notes']) : '',
		];

		return $filterParams;
	}

	protected function additionalFormValidation($model) 
	{
		// Display error if all platform handler is blank
		if (trim($model->handle_tiktok) == '' 
		&& trim($model->handle_instagram) == '' 
		&& trim($model->handle_youtube) == '') 
		{
			Yii::app()->user->setFlash('error', 'Platform handle cannot be all blank!');
			return false;
		} 

		if (trim($model->handle_tiktok) != '') 
		{
			// Validate tiktok hyperlink
			if (trim($model->hyperlink_tiktok) == '') 
			{
				Yii::app()->user->setFlash('error', 'Tiktok Hyperlink cannot be blank!');
				return false;
			}
			else if (!filter_var($model->hyperlink_tiktok, FILTER_VALIDATE_URL))
			{
				Yii::app()->user->setFlash('error', 'Tiktok Hyperlink is not valid!');
				return false;
			}

			// Validate tiktok handle duplicate
			Influencer::model()->findAll();

		} 

		if (trim($model->handle_instagram) != '') 
		{
			// Validate instagram hyperlink
			if (trim($model->hyperlink_instagram) == '') 
			{
				Yii::app()->user->setFlash('error', 'Instagram Hyperlink cannot be blank!');
				return false;
			}
			else if (!filter_var($model->hyperlink_tiktok, FILTER_VALIDATE_URL))
			{
				Yii::app()->user->setFlash('error', 'Instagram Hyperlink is not valid!');
				return false;
			}

			// Validate instagram handle duplicate
		} 

		if (trim($model->handle_youtube) != '') 
		{
			// Validate youtube hyperlink
			if (trim($model->hyperlink_youtube) == '') 
			{
				Yii::app()->user->setFlash('error', 'Youtube Hyperlink cannot be blank!');
				return false;
			}
			else if (!filter_var($model->hyperlink_tiktok, FILTER_VALIDATE_URL))
			{
				Yii::app()->user->setFlash('error', 'Youtube hyperlink is not valid!');
				return false;
			}

			// Validate youtube handle duplicate
		}

		return true;
	}
}
