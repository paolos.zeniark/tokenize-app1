<tr>
    <td><?php echo ($index+1)+(@$_GET['page']*100); ?></td>
    <td><?php echo date('Y-m-d', strtotime($data['date_created'])); ?> @ <?php echo date('h:m A', strtotime($data['date_created'])); ?></td>
    <td><?php echo $data['username']; ?></td>
    <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
    <td><?php echo Influencer::getSeedingPlatformLabel($data['seeding_platform']); ?></td>
    <td><?php echo $data['followers']; ?></td>
    <td><?php echo CHtml::link($data['handle_tiktok'], $data['hyperlink_tiktok'], ['target' => '_blank']); ?></td>
    <td><?php echo CHtml::link($data['handle_instagram'], $data['hyperlink_instagram'], ['target' => '_blank']); ?></td>
    <td><?php echo CHtml::link($data['handle_youtube'], $data['hyperlink_youtube'], ['target' => '_blank']); ?></td>
    <td><?php echo Influencer::getFollowingTierLabel($data['following_tier']); ?></td>
    <td><?php echo Influencer::getDemographicLabel($data['demographic']); ?></td>
    <td><?php echo Influencer::getSegmentLabel($data['segment']); ?></td>
    <td><?php echo $data['notes']; ?></td>
    <td>
        <?php echo CHtml::link('<span class="glyphicon glyphicon-search"></span>', ['view', 'id' => $data['id']]); ?>&nbsp;|&nbsp; 
        <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', ['update', 'id' => $data['id']]); ?>&nbsp;|&nbsp;
        <?php echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $data['id']], ['onclick' => 'return confirm("Are you sure you want to delete this item?")']); ?>
    </td>
</tr>