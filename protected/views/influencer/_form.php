
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'influencer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<div class="col-md-12">
	<?php echo $form->errorSummary($model); ?>
</div>

<div class="col-md-12">
	<?php $this->widget('Flash', array(
		'flashes' => Yii::app()->user->getFlashes()
	)); ?>
</div>

<div class="col-md-6">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'first_name'); ?>
		<?php echo $form->textField($model, 'first_name', ['class' => 'form-control', 'placeholder' => 'Enter First Name']); ?>
	</div>
</div>

<div class="col-md-6">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'last_name'); ?>
		<?php echo $form->textField($model, 'last_name', ['class' => 'form-control', 'placeholder' => 'Enter Last Name']); ?>
	</div>
</div>

<div class="col-md-6">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'seeding_platform'); ?>
		<?php echo $form->dropDownList($model, 'seeding_platform', Influencer::getSeedingPlatformList(), [
			'empty' => 'Select Seedig Platform', 
			'class' => 'form-control']); ?>
	</div>
</div>

<div class="col-md-6">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'followers'); ?>
		<?php echo $form->textField($model, 'followers', ['class' => 'form-control', 'placeholder' => 'Enter Followers Count']); ?>
	</div>
</div>

<div class="col-md-12">
	<hr>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'handle_tiktok'); ?>
		<?php echo $form->textField($model, 'handle_tiktok', ['class' => 'form-control', 'placeholder' => 'Enter TikTok Username']); ?>
	</div>
</div>

<div class="col-md-8">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'hyperlink_tiktok'); ?>
		<?php echo $form->textField($model, 'hyperlink_tiktok', ['class' => 'form-control', 'placeholder' => 'Enter TikTok Link']); ?>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'handle_instagram'); ?>
		<?php echo $form->textField($model, 'handle_instagram', ['class' => 'form-control', 'placeholder' => 'Enter Instagram Username']); ?>
	</div>
</div>

<div class="col-md-8">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'hyperlink_instagram'); ?>
		<?php echo $form->textField($model, 'hyperlink_instagram',['class' => 'form-control', 'placeholder' => 'Enter Instagram Link']); ?>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'handle_youtube'); ?>
		<?php echo $form->textField($model, 'handle_youtube', ['class' => 'form-control', 'placeholder' => 'Enter Youtube Username']); ?>
	</div>
</div>

<div class="col-md-8">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'hyperlink_youtube'); ?>
		<?php echo $form->textField($model, 'hyperlink_youtube', ['class' => 'form-control', 'placeholder' => 'Enter Youtube Link']); ?>
	</div>
</div>

<div class="col-md-12">
	<hr>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'following_tier'); ?>
		<?php echo $form->dropDownList($model, 'following_tier', Influencer::getFollowingTierList(), ['empty' => 'Select Following Tier', 'class' => 'form-control']); ?>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'demographic'); ?>
		<?php echo $form->dropDownList($model, 'demographic', Influencer::getDemographicList(), ['empty' => 'Select Demographic', 'class' => 'form-control']); ?>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'segment'); ?>
		<?php echo $form->dropDownList($model, 'segment', Influencer::getSegmentList(), ['empty' => 'Select Segment', 'class' => 'form-control']); ?>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'notes'); ?>
		<?php echo $form->textArea($model, 'notes', ['class' => 'form-control', 'rows' => 6, 'cols' => 50]); ?>
	</div>
</div>

<div class="col-md-12">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary']); ?>
</div>

<?php $this->endWidget(); ?>