<?php
/* @var $this InfluencerController */
/* @var $model Influencer */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seeding_platform'); ?>
		<?php echo $form->textField($model,'seeding_platform'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'followers'); ?>
		<?php echo $form->textField($model,'followers'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'handle_tiktok'); ?>
		<?php echo $form->textField($model,'handle_tiktok',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'handle_instagram'); ?>
		<?php echo $form->textField($model,'handle_instagram',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'handle_youtube'); ?>
		<?php echo $form->textField($model,'handle_youtube',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'following_tier'); ?>
		<?php echo $form->textField($model,'following_tier'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'demographic'); ?>
		<?php echo $form->textField($model,'demographic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'segment'); ?>
		<?php echo $form->textField($model,'segment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'added_by_user_id'); ?>
		<?php echo $form->textField($model,'added_by_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_updated'); ?>
		<?php echo $form->textField($model,'date_updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->