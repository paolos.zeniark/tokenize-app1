<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Influencer Marketing App</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/navbar-fixed-top.css?v=1" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css?v=1" rel="stylesheet">

    <style>
      body {background: #f5f5f5 !important;}
    </style>
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Influencer Marketing App</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <?php if (!Yii::app()->user->isGuest) : ?>
            <ul class="nav navbar-nav">
              <li><?php echo CHtml::link('Manage', ['influencer/index']); ?></li>
              <li><?php echo CHtml::link('Create', ['influencer/create']); ?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><?php echo CHtml::link('Logout ('. Yii::app()->user->name .')', ['/site/logout']); ?></li>
            </ul>
          <?php else: ?>
            <ul class="nav navbar-nav navbar-right">
              <li><?php echo CHtml::link('Login', ['/site/login']); ?></li>
            </ul>
          <?php endif; ?>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

	<?php echo $content; ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>
  </body>
</html>
