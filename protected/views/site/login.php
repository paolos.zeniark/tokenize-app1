<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="container">
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">Login</div>
				<div class="panel-body">

					<div class="row">
						<div class="col-md-12">
							<?php $this->widget('Flash', array(
								'flashes' => Yii::app()->user->getFlashes()
							)); ?>
						</div>
					</div>

					<p><small>Please fill out the following form with your login credentials:</small></p>

					<div class="form">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
						)); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'username'); ?>
							<?php echo $form->textField($model,'username', ['class' => 'form-control']); ?>
							<?php echo $form->error($model,'username'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'password'); ?>
							<?php echo $form->passwordField($model,'password', ['class' => 'form-control']); ?>
							<?php echo $form->error($model,'password'); ?>
						</div>

						<div class="form-group">
							<?php echo CHtml::submitButton('Login', ['class' => 'btn btn-primary']); ?>
						</div>

						<?php $this->endWidget(); ?>
					</div><!-- form -->
				</div>
			</div>
		</div>
	</div>
</div>

